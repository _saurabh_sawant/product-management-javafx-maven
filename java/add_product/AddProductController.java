package add_product;

import java.sql.Connection;
import java.sql.Statement;

import db_operations.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import show_options.ShowOptions;

public class AddProductController {
	static Statement stmt;
	static Connection con;
	@FXML
	private TextField productname;
	@FXML
	private TextField quantity;
	@FXML
	private TextField price;
	
	@FXML
	private Button add;

	public void addproduct(ActionEvent event) {
		System.out.println(productname.getText());
		System.out.println(quantity.getText());
		System.out.println(price.getText());
		String query = "insert into Product(Name,Quantity,Price) values ('" + productname.getText() + "', '" + quantity.getText() + "','"+ price.getText() + "');";
		System.out.println(query);
		DbUtil.executeQuery(query);
		System.out.println("Event occur add controller "+event.getEventType().getName());
		new ShowOptions().show();
	}

}
