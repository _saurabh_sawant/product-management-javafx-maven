package application;

import db_operations.DbUtil;
import javafx.application.Application;
import javafx.stage.Stage;
import show_options.ShowOptions;
import stage.StageMaster;

public class ApplicationMain extends Application{
	public static void main(String args[]) {
		DbUtil.createDbConnection();
		launch(args);
		
	}
	public void start(Stage primaryStage) {
		StageMaster.setStage(primaryStage);
		new ShowOptions().show();
	}
	

}